import React, { Component } from 'react';

import './TabsContents.scss'

class TabsContents extends Component {
    render() {
        console.log(this.props.activeTabContentInde);
        
        return (
            <div className="tabs-contents">
                {
                    Object.keys(this.props.weather).length > 0 && this.props.weather.days.map((element, index) => {
                        let isActive = false;
                        if (index === this.props.activeTabContentIndex ) {
                            isActive = true;
                        }
                        return (
                            <div key={index} className={isActive ? "tab-content active" : "tab-content"}>
                                <ul  className="list">
                                    <li >
                                        <span className="times-of-day">
                                                Ночь
                                        </span>
                                        <span className="clicon">
                                            <img src={element.day.condition.icon} alt={element.day.condition.text} />
                                        </span>
                                        <span className="precipitation">
                                            {element.day.condition.text}
                                        </span>
                                        <span className="air-temperature">
                                            {element.day.avgtemp_c}
                                        </span>
                      
                                    </li>
                                    <li >
                                        <span className="times-of-day">
                                            Ночь
                                        </span>
                                        <span className="clicon">
                                            <img src={element.day.condition.icon} alt={element.day.condition.text} />
                                        </span>
                                        <span className="precipitation">
                                            {element.day.condition.text}
                                        </span>
                                        <span className="air-temperature">
                                            {element.day.avgtemp_c}
                                        </span>

                                    </li>
                                    <li >
                                        <span className="times-of-day">
                                            Ночь
                                        </span>
                                        <span className="clicon">
                                            <img src={element.day.condition.icon} alt={element.day.condition.text} />
                                        </span>
                                        <span className="precipitation">
                                            {element.day.condition.text}
                                        </span>
                                        <span className="air-temperature">
                                            {element.day.avgtemp_c}
                                        </span>

                                    </li>
                                    <li >
                                        <span className="times-of-day">
                                            Ночь
                                        </span>
                                        <span className="clicon">
                                            <img src={element.day.condition.icon} alt={element.day.condition.text} />
                                        </span>
                                        <span className="precipitation">
                                            {element.day.condition.text}
                                        </span>
                                        <span className="air-temperature">
                                            {element.day.avgtemp_c}
                                        </span>

                                    </li>
                                </ul>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}
export default TabsContents;

