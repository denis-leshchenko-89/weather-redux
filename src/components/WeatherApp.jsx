import React, { Component } from 'react';
//eslint-disable-next-line
import $ from 'jquery';
// import * as moment from 'moment';
import 'font-awesome/css/font-awesome.min.css';

import './WeatherApp.scss';

import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import { getWeather } from "../actions/action";

import { changeMeasurement } from "../actions/action";

import Tabs from "./Tabs/Tabs";
import TabsContents from "./TabsContents/TabsContents";
import  Loader  from "./Loader/Loader";


class WeatherApp extends Component {

  state = {
    activeTabIndex: 0
  }




  componentDidMount() {
    this.getWeather();
  }
  getWeather = () => {
    this.props.getWeather("Запорожье");
  }



  handleTab = (tabIndex) => {
    this.setState({
      activeTabIndex: tabIndex,
    });
  }

  handlerChangeMeasurement = (event) => {

    let measurement = "";

    if (event.target.value === "cesium") {
      measurement = "fahrenheit"

    }
    else {
      measurement = "cesium"
    }


    this.props.changeMeasurement(measurement);


  }


  render() {
    return (
      <div className="weather-app">
        <Loader loader={this.props.loader} />

        <div className="switcher">
          <label className="switch">
            <input type="checkbox" value={this.props.measurement} onChange={this.handlerChangeMeasurement} />
            <span className="slider round"></span>
          </label>
          <span className="heading">{this.props.measurement === "cesium" ? "Цельсия" : "Фаренгейт"}</span>
        </div>
        <div className="search-field"><input type="search" placeholder="Поиск..." /></div>
        <h1>Погода город  {Object.keys(this.props.weather).length > 0 && this.props.weather.location.name} ({Object.keys(this.props.weather).length > 0 && this.props.weather.location.country})</h1>
        <div className="content">
          <Tabs weather={this.props.weather} onCliclTabIndex={this.handleTab} />
          <TabsContents weather={this.props.weather} activeTabContentIndex={this.state.activeTabIndex} />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    weather: state.weatherReducer.weather,
    measurement: state.weatherReducer.measurement,
    loader: state.weatherReducer.loader
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getWeather: getWeather, changeMeasurement: changeMeasurement }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(WeatherApp);
