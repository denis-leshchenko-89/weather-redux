import React, { Component } from 'react';
import './Tabs.scss';

// import { bindActionCreators } from "redux";


import { connect } from "react-redux";



// import { changeMeasurement } from "../actions/action";


class Tabs extends Component {

    constructor(props){
        super(props);
        this.state = {
            tabIndex: 0
        }
    }

    onActiveTab(index)
    {
        this.setState({ tabIndex: index});
        this.props.onCliclTabIndex(index);
    }
  
    render() {
        console.log(this.props.measurement);
        
        return (
            <nav className="tabs">
                {
                    Object.keys(this.props.weather).length > 0 && this.props.weather.days.map((element, index) => {
                        let isActive=false;
                        if (index === this.state.tabIndex){
                            isActive=true;
                        }
                        return (
                            <div className={isActive ? "tab active" : "tab"} key={index} onClick={() => this.onActiveTab(index)}>
                                <img src={element.day.condition.icon} alt={element.day.condition.text} />
                                <div className="date-temp">
                                    <div className="date-dayofweek">
                                        <span className="date">{element.date.day}.{element.date.month}</span>
                                        <span className="dayofweek">{element.date.dayofweek}</span>
                                    </div>
                                    <div className="temp">
                                        <span className="min">
                                            {this.props.measurement === "cesium" ? element.day.mintemp_c : element.day.mintemp_f }
                                        </span>...<span className="max">
                                            {this.props.measurement === "cesium" ? element.day.maxtemp_c : element.day.maxtemp_f}
                                        </span></div>
                                    </div>
                            </div>
                        )
                    })
                }

            </nav>
        );
    }

}



function mapStateToProps(state) {
    return {
        measurement: state.weatherReducer.measurement
    }
}

// function mapDispatchToProps(dispatch) {
//     return bindActionCreators({ changeMeasurement: changeMeasurement }, dispatch);
// }


export default connect(mapStateToProps, null)(Tabs);


