import React, { Component } from 'react'

import "./Loader.scss";

class Loader extends Component {
    render() {
        return (
            <div>
                {this.props.loader ? <div className="loader" ></div>: null } 
            </div>
        );
    }
}

export default Loader;
