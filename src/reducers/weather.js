import * as moment from 'moment';

import 'moment/locale/ru';

const initialState = {
    weather: {
        location:{
            name:"",
            country:""
        },
        days:[]
    },
    search: "",
    measurement: "cesium",
    loader: true
}

function weatherReducer(state = initialState, action) {
    switch (action.type) {
        case 'LOAD_DATA_SUCCESS':
        let stateData={ ...state, weather: action.payload  }

        let date = stateData.weather.forecast.forecastday;
        date.forEach((item) => {
          let check = moment(item.date, 'YYYY/MM/DD');
          let month = check.format('MM');
          let day = check.format('D');
          let dayofweek = check.format('dd');
          item.date = { day: day, month: month, dayofweek: dayofweek }
        })
        return { ...state, weather: { ...action.payload, days: date}};

        case 'CHANGE_MEASUREMENT':

        return { ...state, measurement: action.payload };

        case 'CHANGE_LOADING_STATE':
            return { ...state, loader: action.payload };

        default:
            return state
    }
}

export default weatherReducer;