import { combineReducers } from "redux";

import weatherReducer from "./weather";


const allReducers = combineReducers({
    weatherReducer: weatherReducer,
})

export default allReducers;
