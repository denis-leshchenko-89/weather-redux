export const getWeather = (searchCity) => {
	return (
		(dispatch) => {
			dispatch({ type: 'CHANGE_LOADING_STATE', payload: true })

			fetch(`http://api.apixu.com/v1/forecast.json?key=f868e7abe96c4942a0b93729180603&q=${searchCity}&days=7&lang=ru`)
				.then((response) => {
					return response.json();
				})
				.then((data) => {
					dispatch({ type: 'LOAD_DATA_SUCCESS', payload: data })
				}).catch((error) => {
					dispatch({ type: 'LOAD_DATA_FAILURE', payload: error })
				}).finally(() => {
					dispatch({ type: 'CHANGE_LOADING_STATE', payload: false })
				})

		}
	)

}
export const changeMeasurement = (measurement) => {
	return {
		type: 'CHANGE_MEASUREMENT',
		payload: measurement
	}

}  
