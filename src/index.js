import React from 'react';
import ReactDOM from 'react-dom';
import 'normalize.css';
import './index.scss';
import registerServiceWorker from './registerServiceWorker';

import WeatherApp from './components/WeatherApp.jsx';

import { Provider } from "react-redux";

import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore, applyMiddleware } from 'redux';
import allReducers from "./reducers";
import thunk from 'redux-thunk';


const store = createStore(allReducers, composeWithDevTools(applyMiddleware(thunk)));


ReactDOM.render(
    <Provider store={store}>
        <WeatherApp />
    </Provider>
    , document.getElementById('root'));
registerServiceWorker();
